﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class OptionMenuLogic : MonoBehaviour
{
    [SerializeField]
    Button _startButton;
    // Start is called before the first frame update
    void Start()
    {
        if (_startButton)
        {
            _startButton.Select();
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void OnVideoOptionsClicked()
    {
        Debug.Log("VideoOptionsClicked");
        UIManager.Instance.SetUIState(UIState.VideoOptions);
    }
    public void OnAudioOptionsClicked()
    {
        Debug.Log("AudioOptionsClicked");
        UIManager.Instance.SetUIState(UIState.AudioOptions);

    }
    public void OnBackClicked()
    {
        Debug.Log("QuitClicked");
        UIManager.Instance.SetUIState(UIState.MainMenu);
    }
}
