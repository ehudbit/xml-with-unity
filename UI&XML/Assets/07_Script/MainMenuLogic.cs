﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuLogic : MonoBehaviour
{
    [SerializeField]
    Button _startButton;
    // Start is called before the first frame update
    void Start()
    {
        if(_startButton)
        {
            _startButton.Select();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OnStartClicked()
    {
        Debug.Log("StartClicked");
        SceneManager.LoadScene("GameScene");
    }
    public void OnOptionsClicked()
    {
        Debug.Log("OptionsClicked");
        UIManager.Instance.SetUIState(UIState.OptionsMenu);
    }
    public void OnQuitClicked()
    {
        Debug.Log("QuitClicked");
        Application.Quit();
    }
}
