﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocKey : MonoBehaviour
{
    [SerializeField]
    string _locKey;

    Text _UIText;

    void Start()
    {
        _UIText = GetComponent<Text>();

        if(_UIText && _locKey != "")//Check if locKey is not empty
        {
         _UIText.text = LocalizationManager.Instance.GetLocalizeString(_locKey);
        }
    }
}
