﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;



public enum Languages
{
    English,
    Hebrew
}
public class LocalizationManager : MonoBehaviour
{
    public static LocalizationManager Instance = null;

    [SerializeField]
    Languages _currentLanguages = Languages.Hebrew;

    Dictionary<Languages, TextAsset> _localizationFiles = new Dictionary<Languages, TextAsset>();

    Dictionary<string, string> _localizationData=new Dictionary<string, string>();

    private void Awake()
    {
        if(Instance==null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);

        SetupLocalizationFiles();
        SetupLocalizationData();
    }
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void SetupLocalizationFiles()
    {
        foreach(Languages language in Languages.GetValues(typeof(Languages)))
        {
            string textAssetPath = "Localization/" + language;
            TextAsset textAsset = (TextAsset)Resources.Load(textAssetPath);
            if (textAsset)
            {
                _localizationFiles[language] = textAsset;
                Debug.Log("Text Asset:" + textAsset.name);
            }
            else
            {
                Debug.LogError("Text asset not found:" + textAssetPath);
            }
        }
    }
    void SetupLocalizationData()
    {
        TextAsset textAsset;
        if (_localizationFiles.ContainsKey(_currentLanguages))
        {
            Debug.Log("Selected language found:" + _currentLanguages);
            textAsset = _localizationFiles[_currentLanguages];
        }
        else
        {
            Debug.LogError("Counldnt find localization file for:" + _currentLanguages);
            textAsset = _localizationFiles[Languages.English];
        }
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(textAsset.text);//Load the file we just access

            XmlNodeList entryList = xmlDocument.GetElementsByTagName("Entry");

            foreach(XmlNode entry in entryList)
            {
                if(!_localizationData.ContainsKey(entry.FirstChild.InnerText))
                {
                    Debug.Log("Added Key" + entry.FirstChild.InnerText + " with value " + entry.LastChild.InnerText);
                    _localizationData.Add(entry.FirstChild.InnerText,entry.LastChild.InnerText);
                }
                else
                {
                    Debug.LogError("Duplicate key found for:" + entry.FirstChild.InnerText);
                }

            }
        
    }
    public string GetLocalizeString(string key)
    {
        string localizedString = "";
        if(!_localizationData.TryGetValue(key,out localizedString))
        {
            localizedString = "LOC KEY" + key;
        }

        return localizedString;
    }
}
