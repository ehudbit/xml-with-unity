﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum UIState
{
    MainMenu,
    OptionsMenu,
    VideoOptions,
    AudioOptions
}
[System.Serializable]
public struct UIStateStruct
{
    public UIState _uiState;
    public GameObject _uiObject;
}
public class UIManager : MonoBehaviour
{
    public static UIManager Instance = null;//Now we can access UIManager from anywhere

    [SerializeField]
    List<UIStateStruct> _uiStates = new List<UIStateStruct>();

    Dictionary<UIState, GameObject> _uiStateMap = new Dictionary<UIState, GameObject>();

    UIState _currentUIState = UIState.MainMenu;

    void Awake()
    {
        if(Instance==null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        foreach(UIStateStruct uistateStruct in _uiStates)
        {
            _uiStateMap.Add(uistateStruct._uiState, uistateStruct._uiObject);
        }
        
    }
    public void SetUIState(UIState newState)
    {
        _uiStateMap[_currentUIState].SetActive(false);
        _uiStateMap[newState].SetActive(true);

        _currentUIState = newState;
    }
}
